<?php 
/*
Template Name: explore_the_story
*/
get_header();
get_sidebar();
if(isset($_GET['code']))
{
    $countryCode = $_GET['code'];
}
else
{
    $countryCode = 'na';
}

?>


  <div class="banner">

    <div class="container" data-parallax>

      <div class="banner-text">

        <h1><?php the_field('title_'.$countryCode);?></h1>

        <h5><?php the_field('description_'.$countryCode);?></h5>

        <a href="<?php the_field('button_link_'.$countryCode);?>" class="btn"><span><?php the_field('button_text_'.$countryCode);?></span></a>

      </div>

      <div class="banner-planets">

        <figure class="discover-banners">

          <img src="<?php echo get_template_directory_uri(); ?>/images/discover-austrailia-map.png" />

          <img src="<?php echo get_template_directory_uri(); ?>/images/star.png" class="star" />

        </figure>

      </div>

    </div>

  </div>



  <div class="clouds">



    <div class="clouds-inner">

      <div class="cloud c1">

        <img src="<?php echo get_template_directory_uri(); ?>/images/cloud1.png" />

      </div>

      <div class="cloud c2">

        <img src="<?php echo get_template_directory_uri(); ?>/images/cloud2.png" />

      </div>

      <div class="cloud c3">

        <img src="<?php echo get_template_directory_uri(); ?>/images/cloud3.png" />

      </div>



    </div>



  </div>



  <section class="explore-story-section explore-story-inner" id="quiz">

    <div class="container">

      <p><?php the_field('quiz_description_'.$countryCode);?></p>



      <div class="explore-quiz">

        <h3><?php the_field('quiz_title_'.$countryCode);?></h3>

        <div class="explore-quiz-inner all-result">

            <h4><?php the_field('ques_'.$countryCode);?></h4>

            <form>

              <?php 
                  while( have_rows('ques_answer_'.$countryCode) ): the_row(); 

                  $answer = get_sub_field('answer');
                  $correctAns = get_sub_field('correct');


              ?>
                    <div class="form-group my-radio-group">

                      <label><?php echo $answer; ?>

                      <input type="radio" name="dummy" data-attr="<?php echo $correctAns[0];?>" value="<?php echo $answer; ?>" required />

                      <span class="custom-radio"></span>

                      </label>

                    </div>

              <?php endwhile; ?>
              
              <div class="text-center">

                <button type="button" class="btn check-answer"><span>NEXT</span></button>

              </div>

            </form>

        </div>

      </div>





    </div>



    <div class="text-center explore-place-img">

      <?php $image = get_field('quiz_image_'.$countryCode); ?>

      <img src="<?php echo $image['url'];?>" alt="<?php echo $image['name'];?>" />

    </div>



  </section>
<?php 
    get_footer();
?>

<script>
	
	$('html, body').animate({scrollTop: $("#quiz").offset().top}, 2000);

	jQuery(document).on('click','.check-answer',function(){

      var answer = $('input[name=dummy]:checked').val();
      var correctAns = $('input[name=dummy]:checked').attr('data-attr');
      if(correctAns == 'Yes')
      {
        
        getSecondQuestion();
      }
      else
      {
		  if($("input:radio[name='dummy']").is(":checked")) 
		  {
			jQuery('.text-data').html('You have selected wrong answer');
			 jQuery('[data-pd-popup="exploreStoryPopup"]').show();
		  }
		  else
		  {
			 jQuery('.text-data').html('Kindly check atleast one option');
			 jQuery('[data-pd-popup="exploreStoryPopup"]').show();
		  }
        
      }

  });

  function getSecondQuestion()
  {
      var countrycode = '<?php echo $countryCode; ?>';

      jQuery.ajax({
          url: '<?php echo get_template_directory_uri(); ?>/page-template/explore-the-story-2.php',
          type: 'post',
          dataType: 'html',
          data: {countrycode:countrycode},
          success: function(data)
          {
              $('.all-result').html(data);
          }
      });
  }

  

</script>