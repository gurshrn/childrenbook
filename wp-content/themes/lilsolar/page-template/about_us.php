<?php 
/*
Template Name: about_us
*/
get_header();
get_sidebar();
?>

    <section class="about-page">

      <div class="container">



        <div class="row about-post-row">

          <div class="col-md-10 offset-md-1">

            <div class="about-big-post">

              <figure>

                <?php $image = get_field('image'); ?>

                <img src="<?php echo $image['url'];?>" />

              </figure>

              <h2><?php the_field('title');?></h2>

              <p><?php the_field('description');?></p>

              <a href="<?php the_field('link');?>" class="btn"><span><?php the_field('button_text');?></span></a>

            </div>

          </div>


          <?php 
              while( have_rows('about_us') ): the_row(); 

              $aboutImage = get_sub_field('about_us_image');
              $aboutTitle = get_sub_field('about_us_title');
              $aboutDescription = get_sub_field('about_us_description');
              $aboutLink = get_sub_field('about_us_button_link');
              $aboutBtnText = get_sub_field('about_us_button_text');

          ?>
              <div class="col-md-6">

                  <div class="about-post">

                    <figure>

                      <img src="<?php echo $aboutImage['url']; ?>" />

                    </figure>

                    <h3><?php echo $aboutTitle;?></h3>

                    <p><?php echo $aboutDescription;?></p>

                    <a href="<?php echo $aboutLink; ?>" class="btn"><span><?php echo $aboutBtnText; ?></span></a>

                  </div>

                </div>

            <?php endwhile; ?>
             

             </div>



      </div>

    </section>



    <div class="contact-plants">

      <div class="venus">

      </div>

       <div class="earth">

      </div>

    <div class="mars">

      </div>

      <div class="jupiter">

      </div>

    </div>
<?php 
    get_footer();
?>