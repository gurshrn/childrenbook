<div class="templateContainer productDetails">
	<div class="breadcrumbContainer">
		<div class="container">
			<ul>
				<li><a href="<?php echo get_permalink( get_page_by_title( 'Home' ) );?>">Home</a></li>
				<li><a href="<?php echo get_permalink( get_page_by_title( 'Products' ) ); ?>">Our Products</a></li>
				<li><?php the_field( "title" ); ?></li>
			</ul>
		</div>
	</div>

	<div class="singleProduct">
		<div class="container">
			<div class="single-product clearfix">
				<div id="product-1" class="post-1 product type-product status-publish has-post-thumbnail product_cat-bags product_cat-beauty-wellness product_cat-girls product_cat-shoes first instock shipping-taxable purchasable product-type-simple">
					<div class="product_detail row">
						<div class="col-md-4 col-xs-12 clear_xs">
							<div class="singleProductImages slider_img_productd">
								<div id="mainImage" class="owl-carousel owl-theme">
									<div class="item" data-hash="1">
										<?php 
											if (has_post_thumbnail( $post->ID)) 
												$image =   wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); 
										?> 
										<figure class="mainImage" style="background-image: url('<?php echo $image[0]; ?>');"></figure>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-8 col-xs-12 clear_xs">
							<div class="content_product_detail">
								<?php the_field( "content" ); ?>
							</div>
						</div>
					</div>		
					<div class="tabs clearfix">
						<div class="tabbable">
							<ul class="nav nav-tabs">
								<li class="description_tab active">
									<a href="#tab-information" data-toggle="tab">Nutritional information</a>
								</li>
								<li class="reviews_tab ">
									<a href="#tab-preparation" data-toggle="tab">Preparation & Feeding</a>
								</li>
							</ul>
							<div class="clear"></div>
							<div class=" tab-content">
								<div class="tab-pane active" id="tab-information">
									<?php the_field('nutritional_information');?>


									<div class="tableContaner">
										<?php echo the_field('nutritional_composition_tabel');?>
										<div class="tableWrapper" style="background-color: #dbeaff;">
											<table>
												<thead>
													<tr>
														<th>Minerals/ Vitamins</th>
														<th></th>
														<th></th>
														<th></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Sodium</td>
														<td>mg</td>
														<td>180</td>
														<td>8.5</td>
														<td>23</td>
													</tr>
													<tr>
														<td>Potassium</td>
														<td>mg</td>
														<td>520</td>
														<td>25</td>
														<td>67</td>
													</tr>
													<tr>
														<td>Calcium</td>
														<td>mg</td>
														<td>400</td>
														<td>19</td>
														<td>51</td>
													</tr>
													<tr>
														<td>Magnesium</td>
														<td>mg</td>
														<td>40</td>
														<td>1.9</td>
														<td>5.1</td>
													</tr>
													<tr>
														<td>Phosphorus</td>
														<td>mg</td>
														<td>250</td>
														<td>12</td>
														<td>32</td>
													</tr>
													<tr>
														<td>Chlor de</td>
														<td>mg</td>
														<td>360</td>
														<td>17</td>
														<td>46</td>
													</tr>
													<tr>
														<td>Iron</td>
														<td>mg</td>
														<td>5</td>
														<td>0.24</td>
														<td>0.64</td>
													</tr>
													<tr>
														<td>Zinc</td>
														<td>mg</td>
														<td>8.5</td>
														<td>0.40</td>
														<td>1.1</td>
													</tr>
													<tr>
														<td>Copper</td>
														<td>µg</td>
														<td>350</td>
														<td>17</td>
														<td>45</td>
													</tr>
													<tr>
														<td>Iodine</td>
														<td>µg</td>
														<td>70</td>
														<td>3.3</td>
														<td>9.0</td>
													</tr>
													<tr>
														<td>Manganese</td>
														<td>µg</td>
														<td>60</td>
														<td>2.8</td>
														<td>7.7</td>
													</tr>
													<tr>
														<td>Selenium</td>
														<td>µg</td>
														<td>18</td>
														<td>0.85</td>
														<td>2.3</td>
													</tr>
													<tr>
														<td>Vitamin A</td>
														<td>µg</td>
														<td>555</td>
														<td>26</td>
														<td>71</td>
													</tr>
													<tr>
														<td>Vitamin D3</td>
														<td>µg</td>
														<td>8.5</td>
														<td>1.4</td>
														<td>1.1</td>
													</tr>
													<tr>
														<td>Vitamin E</td>
														<td>mg</td>
														<td>8</td>
														<td>0.38</td>
														<td>1</td>
													</tr>
													<tr>
														<td>Vitamin B1</td>
														<td>µg</td>
														<td>550</td>
														<td>26</td>
														<td>70</td>
													</tr>
													<tr>
														<td>Vitamin B2</td>
														<td>µg</td>
														<td>800</td>
														<td>38</td>
														<td>102</td>
													</tr>
													<tr>
														<td>Vitamin B6</td>
														<td>µg</td>
														<td>450</td>
														<td>21</td>
														<td>58</td>
													</tr>
													<tr>
														<td>Vitamin B12</td>
														<td>µg</td>
														<td>1.5</td>
														<td>0.07</td>
														<td>0.19</td>
													</tr>
													<tr>
														<td>Niacin</td>
														<td>mg</td>
														<td>4</td>
														<td>0.189</td>
														<td>0.45</td>
													</tr>
													<tr>
														<td>Pantothenic Acid</td>
														<td>mg</td>
														<td>3</td>
														<td>0.142</td>
														<td>0.384</td>
													</tr>
													<tr>
														<td>Folic Acid</td>
														<td>µg</td>
														<td>80</td>
														<td>3.8</td>
														<td>10</td>
													</tr>
													<tr>
														<td>Vitamin C</td>
														<td>mg</td>
														<td>100</td>
														<td>4.7</td>
														<td>13</td>
													</tr>
													<tr>
														<td>Vitamin K1</td>
														<td>µg</td>
														<td>55</td>
														<td>2.6</td>
														<td>7</td>
													</tr>
													<tr>
														<td>Biotin</td>
														<td>µg</td>
														<td>1 5</td>
														<td>0.71</td>
														<td>1.9</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="tableWrapper" style="background-color: #c5e6ff;">
											<table>
												<thead>
													<tr>
														<th>Others</th>
														<th></th>
														<th></th>
														<th></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Nucleotide</td>
														<td>mg</td>
														<td>25</td>
														<td>1.2</td>
														<td>32</td>
													</tr>
													<tr>
														<td>Taurine</td>
														<td>mg</td>
														<td>40</td>
														<td>1.9</td>
														<td>5.1</td>
													</tr>
													<tr>
														<td>Choline</td>
														<td>mg</td>
														<td>80</td>
														<td>3.8</td>
														<td>10</td>
													</tr>
													<tr>
														<td>Inositol</td>
														<td>mg</td>
														<td>40</td>
														<td>10.9</td>
														<td>5.1</td>
													</tr>
													<tr>
														<td>L - carnitine</td>
														<td>mg</td>
														<td>10</td>
														<td>0.47</td>
														<td>1.3</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="tab-preparation">
									<h4>Directions for feeds preparation:</h4>

									<ul class="preparationSteps">
										<li>
											<div class="preparationWrapper">
												<div class="preparationInner">
													<div class="preparationImage">
														<figure style="background-image: url(images/preparation1.png)"></figure>
													</div>
												</div>
												<div class="preparationInner">
													<p>Wash your hands with water and soap and dry with a clean towel</p>
												</div>
											</div>
										</li>
										<li>
											<div class="preparationWrapper">
												<div class="preparationInner">
													<div class="preparationImage">
														<figure style="background-image: url(images/preparation2.png)"></figure>
													</div>
												</div>
												<div class="preparationInner">
													<p>Wash feeding bottle, teat and cap and then boil them for 5 minutes</p>
												</div>
											</div>
										</li>
										<li>
											<div class="preparationWrapper">
												<div class="preparationInner">
													<div class="preparationImage">
														<figure style="background-image: url(images/preparation3.png)"></figure>
													</div>
												</div>
												<div class="preparationInner">
													<p>Boil drinking water for 5 minutes and allow cooling to 40oC</p>
												</div>
											</div>
										</li>
										<li>
											<div class="preparationWrapper">
												<div class="preparationInner">
													<div class="preparationImage">
														<figure style="background-image: url(images/preparation4.png)"></figure>
													</div>
												</div>
												<div class="preparationInner">
													<p>Check Feeding table for the exact amount of lukewarm water to pour in the feeding table</p>
												</div>
											</div>
										</li>
										<li>
											<div class="preparationWrapper">
												<div class="preparationInner">
													<div class="preparationImage">
														<figure style="background-image: url(images/preparation5.png)"></figure>
													</div>
												</div>
												<div class="preparationInner">
													<p>Add the exact number of leveled scoops of Actilat 1 then cap the bottle</p>
												</div>
											</div>
										</li>
										<li>
											<div class="preparationWrapper">
												<div class="preparationInner">
													<div class="preparationImage">
														<figure style="background-image: url(images/preparation6.png)"></figure>
													</div>
												</div>
												<div class="preparationInner">
													<p>Shake the bottle well until  the powder is fully  dissolved.</p>
												</div>
											</div>
										</li>
									</ul>

									<div class="tableImageContainer">
										<div class="tableImageWrapper">
											<div class="tableWrapper" style="background-color: #f8f8f8;">
												<h3>Suggested Feeding Table:</h3>
												<table>
													<thead>
														<tr>
															<th>Age of baby</th>
															<th>No Spoons</th>
															<th>Water(ml)</th>
															<th>Meals/ day</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1 week</td>
															<td>2</td>
															<td>60</td>
															<td>6-7</td>
														</tr>
														<tr>
															<td>2 weeks</td>
															<td>3</td>
															<td>90</td>
															<td>6</td>
														</tr>
														<tr>
															<td>3-4 weeks</td>
															<td>4</td>
															<td>120</td>
															<td>5-6</td>
														</tr>
														<tr>
															<td>2 months</td>
															<td>5</td>
															<td>150</td>
															<td>5</td>
														</tr>
														<tr>
															<td>3 months</td>
															<td>6</td>
															<td>180</td>
															<td>5</td>
														</tr>
														<tr>
															<td>4-6 months</td>
															<td>7</td>
															<td>210</td>
															<td>4-5</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="tableImageWrapper">
											<img src="images/tableImage.jpg" alt="Table Image">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>