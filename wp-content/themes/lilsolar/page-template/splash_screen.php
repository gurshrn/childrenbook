<?php 
/*
Template Name: splash_screen
*/
global $post;
get_header();
?>
	<body class="intro-page">

		<div class="intro" id="container">
			<div class="container">
				<div class="logo">

					<?php $image = get_field('logo'); ?>

					<a href="<?php the_field('header_logo_link','options');?>"><img src="<?php echo $image['url'];?>" alt="Logo" /></a>
				</div>
				<div class="action-btn">
					<ul>
						<li>
							<button type="button" class="btn SkipBtn" data-href="<?php the_field('intro_link');?>" ><span>Skip Intro</span></button>
						</li>
						<li>
							<button type="button" class="btn StartJourney" data-href="<?php the_field('journey_link');?>"><span>Start The Journey</span></button>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="planets">
			<div class="sun">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/sun.png" alt="Sun" />
			</div>
			<div class="mercury"></div>
			<div class="venus"></div>
			<div class="earth"></div>
			<div class="mars"></div>
			<div class="jupiter"></div>
			<div class="saturn">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/saturn.png" alt="saturn" />
			</div>
			<div class="uranus"></div>
			<div class="neptune"></div>
			<div class="pluto"></div>
		</div>
		<canvas id="starCanvas"></canvas>
<?php get_footer();?>
