<?php 
/*
Template Name: home
*/
global $post;
get_header();
get_sidebar();
?>

    <div class="banner">

      <div class="container" data-parallax>

        <div class="banner-text">

          <h1><?php the_field('title');?></h1>

          <h5><?php the_field('sub_title');?></h5>

          <a href="<?php the_field('button_link'); ?>" class="btn"><span><?php the_field('button_text'); ?></span></a>

        </div>

        <div class="banner-planets">

          <div class="jupiter planet">

          </div>



          <div class="saturn planet">

            <img src="<?php echo get_template_directory_uri(); ?>/images/saturn.png" />

          </div>



          <div class="uranus planet">

          </div>

        </div>

      </div>

    </div>



    <div class="clouds">



      <div class="clouds-inner">

        <div class="cloud c1">

          <img src="<?php echo get_template_directory_uri(); ?>/images/cloud1.png" />

        </div>

        <div class="cloud c2">

          <img src="<?php echo get_template_directory_uri(); ?>/images/cloud2.png" />

        </div>

        <div class="cloud c3">

          <img src="<?php echo get_template_directory_uri(); ?>/images/cloud3.png" />

        </div>



      </div>



    </div>



    <section class="explore-story-section">

      <div class="container">

        <h2><?php the_field('explore_story_title');?></h2>

        <p><?php the_field('explore_story'); ?></p>
      </div>

      <div class="explore-map-cover">

        <h4><?php the_field('explore_map'); ?></h4>
		
		<?php 
			$cookies = $_COOKIE['quiz'];
			$cookie = stripslashes($cookies); 
			$cookieArray = json_decode($cookie);
		?>



        <div class="explore-map">

          <div class="explore-map-inner">

            <div class="continents-list">

              <div class="continent nor_Amer">

                <figure>

                  <img src="<?php echo get_template_directory_uri(); ?>/images/NorthAmerica.png" />

                  <a href="<?php echo ((!empty($cookieArray) && $cookieArray->na == 'true')?get_site_url().'/discover-north-america':'javascript:void(0)'); ?>">

                    <figcaption>

                      <span>North America</span>

                      <i class="fa fa-hand-o-up" aria-hidden="true"></i>

                    </figcaption>

                  </a>

                </figure>



              </div>

              <div class="continent sou_Amer <?php echo ((!empty($cookieArray) && $cookieArray->sa == 'true')?'active':''); ?>">

                <figure>

                  <img src="<?php echo get_template_directory_uri(); ?>/images/SouthAmerica.png" /><img src="<?php echo get_template_directory_uri(); ?>/images/SouthAmericaHover.png" />

                  <a href="<?php echo ((!empty($cookieArray) && $cookieArray->sa == 'true')?get_site_url().'/discover-south-america':'javascript:void(0)'); ?>">

                    <figcaption>

                      <span>South America</span>

                      <i class="fa <?php echo ((!empty($cookieArray) && $cookieArray->sa == 'true')?'fa-hand-o-up':'fa-lock'); ?> aria-hidden="true"></i>

                    </figcaption>

                  </a>

                </figure>





              </div>



              <div class="continent antarctica <?php echo ((!empty($cookieArray) && $cookieArray->ant == 'true')?'active':''); ?>">

                <figure>

                  <img src="<?php echo get_template_directory_uri(); ?>/images/Antarctica.png" /><img src="<?php echo get_template_directory_uri(); ?>/images/AntarcticaHover.png" />

                  <a href="<?php echo ((!empty($cookieArray) && $cookieArray->ant == 'true')?get_site_url().'/discover-antartica':'javascript:void(0)'); ?>">

                    <figcaption>

                      <span>Antarctica</span>

                      <i class="fa <?php echo ((!empty($cookieArray) && $cookieArray->ant == 'true')?'fa-hand-o-up':'fa-lock'); ?>" aria-hidden="true"></i>

                    </figcaption>

                  </a>

                </figure>



              </div>

              <div class="continent europe <?php echo ((!empty($cookieArray) && $cookieArray->eur == 'true')?'active':''); ?>">

                <figure>

                  <img src="<?php echo get_template_directory_uri(); ?>/images/Europe.png" /><img src="<?php echo get_template_directory_uri(); ?>/images/EuropeHover.png" />

                  <a href="<?php echo ((!empty($cookieArray) && $cookieArray->eur == 'true')?get_site_url().'/discover-europe':'javascript:void(0)'); ?>">

                    <figcaption>

                      <span>Europe</span>

                      <i class="fa <?php echo ((!empty($cookieArray) && $cookieArray->eur == 'true')?'fa-hand-o-up':'fa-lock'); ?>" aria-hidden="true"></i>

                    </figcaption>

                  </a>

                </figure>



              </div>

              <div class="continent asia <?php echo ((!empty($cookieArray) && $cookieArray->asia == 'true')?'active':''); ?>">

                <figure>

                  <img src="<?php echo get_template_directory_uri(); ?>/images/Asia.png" /><img src="<?php echo get_template_directory_uri(); ?>/images/AsiaHover.png" />

                  <a href="<?php echo ((!empty($cookieArray) && $cookieArray->asia == 'true')?get_site_url().'/discover-asia':'javascript:void(0)'); ?>">

                    <figcaption>

                      <span>Asia</span>

                      <i class="fa <?php echo ((!empty($cookieArray) && $cookieArray->asia == 'true')?'fa-hand-o-up':'fa-lock'); ?>" aria-hidden="true"></i>

                    </figcaption>

                  </a>

                </figure>



              </div>

              <div class="continent africa <?php echo ((!empty($cookieArray) && $cookieArray->africa == 'true')?'active':''); ?>">

                <figure>

                  <img src="<?php echo get_template_directory_uri(); ?>/images/Africa.png" /><img src="<?php echo get_template_directory_uri(); ?>/images/AfricaHover.png" />

                  <a href="<?php echo ((!empty($cookieArray) && $cookieArray->africa == 'true')?get_site_url().'/discover-africa':'javascript:void(0)'); ?>">


                    <figcaption>

                      <span>Africa</span>

                      <i class="fa <?php echo ((!empty($cookieArray) && $cookieArray->africa == 'true')?'fa-hand-o-up':'fa-lock'); ?>" aria-hidden="true"></i>

                    </figcaption>

                  </a>

                </figure>



              </div>

              <div class="continent australia <?php echo ((!empty($cookieArray) && $cookieArray->aus == 'true')?'active':''); ?>">



                <figure>

                  <img src="<?php echo get_template_directory_uri(); ?>/images/Australia.png" /><img src="<?php echo get_template_directory_uri(); ?>/images/AustraliaHover.png" />

                  <a href="<?php echo ((!empty($cookieArray) && $cookieArray->aus == 'true')?get_site_url().'/discover-australia':'javascript:void(0)'); ?>">

                    <figcaption>

                      <span>Australia</span>

                      <i class="fa <?php echo ((!empty($cookieArray) && $cookieArray->aus == 'true')?'fa-hand-o-up':'fa-lock'); ?> aria-hidden="true"></i>

                    </figcaption>

                  </a>

                </figure>



              </div>





            </div>





          </div>

        </div>



      </div>

    </section>

<?php 
    get_footer();
?>