<?php 
/*
Template Name: contact_us
*/
get_header();
get_sidebar();
?>

		<section class="contact-page">
			
      <div class="container">
				
        <div class="for-inquiries">

          <?php $image = get_field('image'); ?>
					
          <figure style="background-image:url('<?php echo $image['url'];?>')"></figure>
					
          <h4><?php the_field('inquiry_title');?></h4>
						
            <p><?php the_field('inquiry_description');?></p>
			
            <p><?php the_field('contact_information');?></p>
					
          <h4><?php the_field('press_coverage_title');?></h4>
					
          <ul>
            <?php 
              while( have_rows('press_coverage_description') ): the_row(); 

              $pressTitle = get_sub_field('press_coverage_desc_title');
              $pressLink = get_sub_field('press_coverage_desc_link');
            ?>
						<li>
							<a href="<?php echo $pressLink;?>" target="_blank"><?php echo $pressTitle;?></a>
						</li>
						<?php endwhile; ?>
					</ul>
				</div>
				<h2><?php the_field('title');?></h2>

				<p><?php the_field('description');?></p>

        <h4><strong>The Universe.</strong> </h4>

				<div class="teamSlider">

					<div class="team-slider-for">

            <?php 

              $images = get_field('slider');

              if( $images ): ?>
                  
                      <?php 
                        foreach( $images as $image ): ?>

                         <figure style="background-image:url('<?php echo $image['url']; ?>');"> </figure>
                         
                      <?php  endforeach;  ?>
                 
              <?php endif; ?>


          </div>





          <div class="team-slider-nav">

            <?php 

              $images = get_field('slider');

              if( $images ): ?>
                  
                      <?php 
                        foreach( $images as $image ): ?>

                         <figure style="background-image:url('<?php echo $image['url']; ?>');"> </figure>
                         
                      <?php  endforeach;  ?>
                 
              <?php endif; ?>
          </div>
        </div>



        <div class="get-in-touch-form">

          <h2><?php the_field('contact_form_title');?></h2>

          <?php echo do_shortcode( '[contact-form-7 id="581" title="Contact form 1"]' ); ?>

        </div>



      </div>

    </section>



    <div class="contact-plants">

      <div class="venus">

      </div>

       <div class="earth">

      </div>

    <div class="mars">

      </div>

      <div class="jupiter">

      </div>

    </div>

<?php 
    get_footer();
?>