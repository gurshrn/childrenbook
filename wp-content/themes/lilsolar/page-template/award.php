<?php 
/*
Template Name: award
*/
get_header();
get_sidebar();
?>


    <section class="shop-page">

      <div class="container">

        <div class="shop-img">

          <figure>

            <?php $image = get_field('image'); ?>

            <img src="<?php echo $image['url'];?>" />

          </figure>

          <a href="<?php the_field('button_link');?>" class="btn mt-4"><span><?php the_field('button_text');?></span></a>

        </div>

          <div class="shop-text">

            <h2><?php the_field('title');?></h2>

            <p><?php the_field('description');?></p>



            <a href="javascript:void(0)" data-pd-popup-open="popupNew" class="download-link mt-5"><i class="fa fa-download" aria-hidden="true"></i> DOWNLOAD YOUR SUPER certificate!</a>
			
			


          </div>



      </div>

    </section>

<?php  get_footer();?>

<div class="popup" data-pd-popup="popupNew">

    <div class="popup-inner">

        <h5>Where do you want us to send the certificate?</h5>

        <form method="post" action="<?php echo get_site_url().'/award/';?>" id="as-fdpf-form">

          <div class="form-group">

            <input type="text" class="form-control"  name="first_name" placeholder="First Name*" required/>

          </div>

          <div class="form-group">

            <input type="text" class="form-control" name="last_name" placeholder="Last Name*" required/>

          </div>

          <div class="form-group">

            <input type="text" class="form-control" name="email" placeholder="Email (optional)"/>

          </div>

          <div class="text-center">

            <button type="submit" class="btn generate-pdf" name="generate_posts_pdf"><span>submit</span></button>

          </div>

        </form>



        <a class="popup-close" data-pd-popup-close="popupNew" href="javascript:void(0)"> </a>

    </div>

</div>

<script>

    jQuery(document).on('click','.popup-close',function(){
        jQuery('.popup').css('display','none');
    });
	
	// jQuery(document).on('click','.generate-pdf',function(){
		// var data = {action: 'generatePdf',code: 'test'};
		// var ajaxurl =  '<?php echo get_site_url();?>/wp-admin/admin-ajax.php';
		// jQuery.post(ajaxurl, data, function(response) {
			//window.location.href = url;
		// });
	// });
	

</script>

