<?php 
/*
Template Name: explore_the_story-2
*/
include ('../../../../wp-load.php');
$page = get_page_by_path('explore-story');
$countryCode = $_POST['countrycode'];
$nextCountryLink = get_field('next_country_'.$countryCode,$page->ID);
$nextCountryCode = get_field('select_next_country_'.$countryCode,$page->ID);
$siteUrl = get_site_url().'/award/';
?>

  

    

      <h4><?php the_field('ques2_'.$countryCode,$page->ID);?></h4>

        <form action="award.html">

            <?php 
                  while( have_rows('ques2_answer_'.$countryCode,$page->ID) ): the_row(); 

                  $answer = get_sub_field('answer2');
                  $correctAns = get_sub_field('correct2');
            ?>

                    <div class="form-group my-radio-group">

                      <label><?php echo $answer; ?>

                      <input type="radio" name="second-ques" data-attr="<?php echo $correctAns[0];?>" value="<?php echo $answer; ?>" required />

                      <span class="custom-radio"></span>

                      </label>

                    </div>

              <?php endwhile; ?>

              <div class="text-center">

                  <button type="button" class="btn check-second-ques"><span>UNLOCK THE NEXT COUNTRY</span></button>

              </div>

    </form>


<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script>

  jQuery(document).on('click','.check-second-ques',function(){

      var answer = $('input[name=second-ques]:checked').val();
      var correctAns = $('input[name=second-ques]:checked').attr('data-attr');
      
      if(correctAns == 'Yes')
      {
        //jQuery.cookie('quiz_<?php echo $nextCountryCode; ?>', 'true');
        checkSecondQuestion();
      }
      else
      {
        if($("input:radio[name='second-ques']").is(":checked")) 
		  {
			 jQuery('.text-data').html('You have selected wrong answer');
			 jQuery('[data-pd-popup="exploreStoryPopup"]').show();
		  }
		  else
		  {
			  jQuery('.text-data').html('Kindly check atleast one option');
        jQuery('[data-pd-popup="exploreStoryPopup"]').show();
		  }
      }

  });

  function checkSecondQuestion()
  {

      var countrycode = '<?php echo $countryCode; ?>';
      var nextCountry = '<?php echo $nextCountryCode; ?>';
      var url = '<?php echo $nextCountryLink;?>';
      var siteurl = '<?php echo $siteUrl;?>';
	 if(url == siteurl)
	  {
		  
		jQuery('.text-data').html("Congratulations! You did it! It's time to collect your reward.");
		jQuery('.pop-up-button').html('<a href="'+url+'" class="btn"><span>Next</span></a>');
		
		jQuery('[data-pd-popup="exploreStoryPopup"]').show();
	  }
	  else
	  {
		  var data = {action: 'setsookies',code: nextCountry};
		  var ajaxurl =  '<?php echo get_site_url();?>/wp-admin/admin-ajax.php';
		  jQuery.post(ajaxurl, data, function(response) {
			  //window.location.href = url;
		  });
		  
	  }
      
      


      
      /*jQuery.ajax({
        type: "GET",
        action: 'setCookies',
        data:{nextCountry:nextCountry},
        dataType: "json",
        success: function (data)
        {
          //window.location = data.url;
        }
      });*/
     // 
  }
  


  

</script>



     