<?php 
/*
Template Name: about_energy_vibrations
*/
get_header();
get_sidebar();
?>


    <section class="about-page about-page-inner" style="background-image:url('<?php echo get_template_directory_uri(); ?>/images/about-back.jpg');">



      <div class="container">



        <div class="row align-self-center">



          <div class="col-md-5 pr-5 align-self-center">

            <figure>

              <?php $image = get_field('image'); ?>

              <img src="<?php echo $image['url'];?>" />

            </figure>

          </div>



          <div class="col-md-7 align-self-center">

            <div class="about-dark-element">

              <h2><?php the_field('title');?></h2>

              <h3><?php the_field('sub_title');?></h3>

              <p><?php the_field('description');?></p>

            </div>



          </div>









        </div>



        <div class="navigation-btn">

          <a href="<?php the_field('previous_link');?>" class="btn"><span>Previous</span></a>

          <a href="<?php the_field('next_link');?>" class="btn"><span>Next</span></a>

        </div>







      </div>

    </section>
<?php 
    get_footer();
?>