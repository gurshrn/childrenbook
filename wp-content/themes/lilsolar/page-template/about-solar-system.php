<?php 
/*
Template Name: about_solar_system
*/
get_header();
get_sidebar();
?>

    <section class="about-page about-page-inner">

      <div class="container">



        <div class="row align-self-center">



          <div class="col-md-5 pl-5 align-self-center">

            <figure>

              <?php $image = get_field('image'); ?>

              <img src="<?php echo $image['url'];?>" />

            </figure>

          </div>

          <div class="col-md-7 align-self-center">

            <h2><?php the_field('title');?></h2>

            <h3><?php the_field('sub_title');?></h3>

            <p><?php the_field('description');?></p>

          </div>



        </div>



        <div class="navigation-btn">

          <a href="<?php the_field('previous_link');?>" class="btn"><span>Previous</span></a>

          <a href="<?php the_field('next_link');?>" class="btn"><span>Next</span></a>

        </div>







      </div>

    </section>



    <div class="contact-plants">

      <div class="venus">

      </div>

       <div class="earth">

      </div>

    <div class="mars">

      </div>

      <div class="jupiter">

      </div>

    </div>

<?php 
    get_footer();
?>