<?php 
/*
Template Name: discover_north_america
*/
get_header();
get_sidebar();

?>

  <div class="banner">

    <div class="container" data-parallax>

      <div class="banner-text">

        <h1><?php the_field('title');?></h1>

        <h5><?php the_field('sub_title');?></h5>

        <a href="javascript:void(0)" class="btn discover-page"><span><?php the_field('button_text');?></span></a>

      </div>

      <div class="banner-planets">

        <figure class="discover-banners">

        <img src="<?php echo get_template_directory_uri(); ?>/images/discover-northAmerica-map.png" />

        <img src="<?php echo get_template_directory_uri(); ?>/images/star.png" class="star" />

      </figure>

      </div>

    </div>

  </div>



  <div class="clouds">



    <div class="clouds-inner">

      <div class="cloud c1">

        <img src="<?php echo get_template_directory_uri(); ?>/images/cloud1.png" />

      </div>

      <div class="cloud c2">

        <img src="<?php echo get_template_directory_uri(); ?>/images/cloud2.png" />

      </div>

      <div class="cloud c3">

        <img src="<?php echo get_template_directory_uri(); ?>/images/cloud3.png" />

      </div>



    </div>



  </div>



  <section class="explore-story-section explore-story-inner" id="Story">

    <div class="container">

      <h2><?php the_field('story_title');?></h2>

      <p><?php the_field('story');?></p>



      <div class="text-center text-uppercase next-link">

        <p>
          <?php $link = get_field('link'); ?>

          <strong>
            <?php the_field('link_title');?></strong> <a href="<?php echo $link.'?code='.get_field('country');?>"><?php the_field('link_text');?><span>&gt;&gt;&gt;</span></a>

        </p>

      </div>



    </div>



    <div class="text-center explore-place-img">

      <?php $image = get_field('image'); ?>

      <img src="<?php echo $image['url'];?>" alt="North America" />

    </div>



  </section>
<?php 
    get_footer();
?>