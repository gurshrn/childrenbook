<?php 
/*
Template Name: book-shop
*/
get_header();
get_sidebar();
global $product;
?>


    <section class="shop-page">

      <div class="container">

         
                  <div class="shop-img">

                    <figure>
					
						<?php $image = get_field('image'); ?>

                      <img src="<?php echo $image['url'];?>" />

                    </figure>

                  </div>

                  <div class="shop-text">

                    <h2><?php the_field('title');?></h2>

                    <p><?php the_field('description');?></p>

                    <a href="<?php the_field('button_link');?>" target="_blank" class="btn mt-4">
                      <span><?php the_field('button_text');?></span>
                    </a>

                  </div>

               


      </div>

    </section>

<?php 
    get_footer();
?>

</body>



</html>

