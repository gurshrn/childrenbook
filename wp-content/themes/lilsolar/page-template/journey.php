<?php 
/*
Template Name: journey
*/
global $post;
get_header();
get_sidebar();
$url = get_template_directory_uri();

?>

    <section class="lil-face-cover">





      <div class="container">

          <iframe src="<?php echo get_template_directory_uri(); ?>/assets/laughing.mp3"  allow="autoplay" id="audio"></iframe>

          <audio id="audiofile2" controls ><source src="<?php echo get_template_directory_uri(); ?>/assets/StarWars.mp3" type="audio/mp3" /></audio>

            <div class="audio-btn">

                <i class="fa fa-pause"></i>

            </div>

            <div class="audio-btn-mute">

              <i class="fa fa-volume-up"></i>

          </div>

            <p class="line subs" id="subtitles2"></p>

      </div>



      <div class="action-btn">

        <ul>

          <li>

            <button type="button" class="btn SkipBtn" data-href="<?php the_field('link');?>"><span>Skip Intro</span></button>

          </li>

        </ul>

      </div>



    </section>



    <div class="contact-plants">

      <div class="venus">

      </div>

      <div class="mars">

      </div>

      <div class="jupiter">

      </div>

    </div>


<?php 
    get_footer();
?>
<script>

      var x = new audioSync({

          audioPlayer: 'audiofile2',

          subtitlesContainer: 'subtitles2',

          subtitlesFile: '<?php echo $url.'/page-template/MIB2-subtitles-pt-BR.vtt'?>',

      });    

</script>

</body>
</html>
