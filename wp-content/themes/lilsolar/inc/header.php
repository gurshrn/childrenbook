<!doctype html>

<html>
	<head>
		<meta charset="utf-8">
		<title>Lil Solar's - Book of Superpowers | Welcome</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<link rel="shortcut icon" href="favicon.png" sizes="32x32" type="image/x-icon">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
	</head>
	<body>
		<div class="overlay" id="overlay">
			<nav class="overlay-menu">
				<ul>
					<?php 
						$walker = new Menu_With_Description; 
						$menu = wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'walker' => $walker ) ); 
					?>
				</ul>
			</nav>
		</div>