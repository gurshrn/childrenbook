<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
	<footer>
		<div class="container">
			<p><?php the_field( 'footer_description','options' ); ?></p>
		</div>
	</footer>
</main>
<canvas id="starCanvas"></canvas>
<!-- Modal -->
<div id="myModal" class="popup" data-pd-popup="exploreStoryPopup">
  <div class="popup-inner">
				<p class="text-data"></p>
				<a class="popup-close" data-pd-popup-close="exploreStoryPopup" href="#"> </a>
				
				<div class="pop-up-button">
					
				</div>
  </div>
</div>

<?php wp_footer(); ?>

	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/popper.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/stars.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/mouseMove.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/typed.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/app.js"></script>
	
