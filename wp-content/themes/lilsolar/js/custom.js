(function ($) {

  'use strict';



  $('document').ready(function () {
	  
	  
	  $('.discover-page').click(function(){
		  $('html, body').animate({scrollTop: $("#Story").offset().top}, 2000);
	  });



    // Intro Page animation

    var $body = $("body");

    setTimeout(function () {

      $body.addClass("loadingEnd");

    }, 1500);



    //Menu

    $("#hamburger").click(function (e) {

      e.preventDefault();

      $("body").toggleClass("toggled");

      $('body .overlay').toggleClass('open');

    });

    var trigger = $('#hamburger'),

      isClosed = true;

    trigger.click(function () {

      burgerTime();

    });



    function burgerTime() {

      if (isClosed == true) {

        trigger.removeClass('is-open');

        trigger.addClass('is-closed');

        isClosed = false;

      } else {

        trigger.removeClass('is-closed');

        trigger.addClass('is-open');

        isClosed = true;

      }

    }



    //Explore Map animation

    var $animation_elements = $(".explore-map");

    var $window = $(window);



    function check_if_in_view() {

      var window_height = $window.height();

      var window_top_position = $window.scrollTop();

      var window_bottom_position = window_top_position + window_height;

      $.each($animation_elements, function () {

        var $element = $(this);

        var element_height = $element.outerHeight();

        var element_top_position = $element.offset().top;

        var element_bottom_position = element_top_position + element_height;

        //check to see if this current container is within viewport

        if (

          element_bottom_position >= window_top_position &&

          element_top_position <= window_bottom_position

        ) {

          $element.addClass("start-animation");

        } else {

          $element.removeClass("start-animation");

        }

      });

    }

    $window.on("scroll resize", check_if_in_view);

    $window.trigger("scroll");



    //Delay the reload

    $(function () {

      $(".action-btn .SkipBtn").click(function () {

        var href = $('.SkipBtn').attr('data-href');

        $("body").addClass("reloadAnimation");

        var seconds = 5;

        setInterval(function () {

          seconds--;

          if (seconds == 0) {

            window.location = href;

          }

        }, 520);

      });

    });



    $(function () {

      $(".action-btn .StartJourney").click(function () {

        var href = $('.StartJourney').attr('data-href');
        

        $("body").addClass("lilSolarAnimation");

        var seconds = 5;

        setInterval(function () {

          seconds--;

          if (seconds == 0) {

            window.location = href;

          }

        }, 520);

      });

    });



    // Contact Page - Team slider

    var teamSlider = $(".teamSlider");

    if (teamSlider.length > 0) {

      $('.team-slider-for').slick({

        slidesToShow: 1,

        slidesToScroll: 1,

        arrows: false,

        fade: true,

        asNavFor: '.team-slider-nav'

      });





      $('.team-slider-nav').slick({

        slidesToShow: 5,

        slidesToScroll: 1,

        asNavFor: '.team-slider-for',

        centerMode: false,

        focusOnSelect: true

      });

    }



    //On scroll animation



    function parallaxFix() {

      var parallax = jQuery("[data-parallax]");

      jQuery(window).on("scroll", function () {

        var ww = jQuery(window).width(),

          x = jQuery(window).scrollTop();

        parallax.css({

          "transform": "translateY(" + parseInt(+x / 1.5) + "px)"

        });

        // if (ww < 768) {

        //   parallax.css(

        //     "background-position",

        //     "center " + parseInt(-x / 50) + "px"

        //   );

        // }

      });

    }

    parallaxFix();



  });





  // Lil Solar Page animation

  var $Lilbody = $("body");

  setTimeout(function () {

    $Lilbody.addClass("startLil");

  }, 1800);



  // var $lilFaceMov = $(".lil-face-cover .container .row");

  // setInterval(function() {

  //   $lilFaceMov.toggleClass("move");

  // }, 30000);





  //   var typedCheck = $("#typed");

  // if (typedCheck.length > 0) {

  //   var typed = new Typed('#typed', {

  //     stringsElement: '#typed-strings',

  //     startDelay: 2600,

  //     typeSpeed: 30,

  //     fadeOut: true,

  //     fadeOutDelay: 0,

  //   });

  // }



  // $("#mainAudio").stop("true").delay('1000').queue(function() {

  //     $(this).html('<source src="assets/StarWars.mp3" type="audio/mp3" />');

  // });







  var aud = $('audio')[0],

    status = false;

  $(window).on('load', function () {

    if($("audio").length > 0) {

      $('.audio-btn').addClass('icon-stop');



      setTimeout(function () {

        if (status == false) {

          aud.play();

          console.log("Start");

        }

      }, 3000);

    }

  })



  $('.audio-btn').on('click', function () {

    if (aud.paused) {

      aud.play();

      status = false;

      $('.audio-btn').removeClass('icon-play');

      $('.audio-btn').addClass('icon-stop');

    } else {

      aud.pause();

      status = true;

      $('.audio-btn').removeClass('icon-stop');

      $('.audio-btn').addClass('icon-play');

    }











  })



  $('.audio-btn-mute').on('click', function () {

    aud.muted = !aud.muted;

    $(".audio-btn-mute").toggleClass('muted');

    return false;

  })









  //Popup

  //----- OPEN

  $('[data-pd-popup-open]').on('click', function (e) {

    var targeted_popup_class = jQuery(this).attr('data-pd-popup-open');

    $('[data-pd-popup="' + targeted_popup_class + '"]').fadeIn(100);



    e.preventDefault();

  });



  //----- CLOSE

  $('[data-pd-popup-close]').on('click', function (e) {

    var targeted_popup_class = jQuery(this).attr('data-pd-popup-close');

    $('[data-pd-popup="' + targeted_popup_class + '"]').fadeOut(200);



    e.preventDefault();

  });


  var banner = $('.banner');
var range = 600;

$(window).on('scroll', function () {
  
  var scrollTop = $(this).scrollTop(),
      height = banner.outerHeight(),
      offset = height / 2,
      calc = 1 - (scrollTop - offset + range) / range;

      banner.css({ 'opacity': calc });

  if (calc > '0.5') {
    banner.css({ "opacity": "1", "pointer-events": "auto" });
  } else if ( calc < '0' ) {
    banner.css({ "opacity": "0", "pointer-events": "none" });
  }
  
});

  //Avoid pinch zoom on iOS

  document.addEventListener('touchmove', function (event) {

    if (event.scale !== 1) {

      event.preventDefault();

    }

  }, false);

})(jQuery)