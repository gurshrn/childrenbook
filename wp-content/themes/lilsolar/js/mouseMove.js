var $wrap = $('.intro .logo a img'),
  $bannerPlanets = $('.banner .banner-planets'),
  $Book = $('.shop-img img'),
  $about = $('figure img'),
  lFollowX = 5,
  lFollowY = 10,
  x = 0,
  y = 0,
  friction = 1 / 12;

function animate() {
  x += (lFollowX - x) * friction;
  y += (lFollowY - y) * friction;


  $wrap.css({
    'transform': ' perspective(600px) rotateY(' + -x + 'deg) rotateX(' + y + 'deg)'
  });
  $bannerPlanets.css({
    'transform': ' perspective(900px) rotateZ(' + y + 'deg)'
  });
  $Book.css({
    'transform': ' perspective(900px) rotateY(' + y + 'deg)'
  });
  // $about.css({
  //   'transform': ' perspective(900px) rotateY(' + x + 'deg) rotateX(' + y + 'deg)'
  // });

  window.requestAnimationFrame(animate);
}

$(window).on('mousemove click', function(e) {

  var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
  var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
  lFollowX = (18 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
  lFollowY = (12 * lMouseY) / 100;

});

animate();
