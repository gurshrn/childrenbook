<?php
/*
* Plugin Name: Atomic Smash FPDF Tutorial
* Description: A plugin created to demonstrate how to build a PDF document from WordPress posts.
* Version: 1.0
* Author: Anthony Hartnell
* Author URI: https://www.atomicsmash.co.uk/blog/author/anthony/
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include( 'atomicsmash-pdf-helper-functions.php');
$pdf = new PDF_HTML();

if( isset($_POST['generate_posts_pdf'])){
    output_pdf();
}

add_action( 'admin_menu', 'as_fpdf_create_admin_menu' );
function as_fpdf_create_admin_menu() {
    $hook = add_submenu_page(
        'tools.php',
        'Atomic Smash PDF Generator',
        'Atomic Smash PDF Generator',
        'manage_options',
        'as-fdpf-tutorial',
        'as_fpdf_create_admin_page'
    );
}

function output_pdf() 
{
	require('class.phpmailer.php');
	global $pdf;
	
	$filename=wp_upload_dir();
	$path = $filename['basedir'].'/pdf/certificate.pdf';
	$subject = 'Well Done!';
	$message = "Congratulations on completing all quiz.";
	$to = 'gurjeevan.kaur@imarkinfotech.com';
	$attachments = array( WP_CONTENT_DIR . '/uploads/pdf/certificate.pdf' );
	
	$firstName = $_POST['first_name'];
	$lastName = $_POST['last_name'];
	$fullName = strtoupper($firstName.' '.$lastName);
	$currentDate = date('d M Y');
	
	
	$title_line_height = 10;
	$content_line_height = 8;

	$pdf->AddPage('L');
	$pdf->Image(get_site_url().'/wp-content/plugins/atomicsmash-pdf-tutorial/background.jpg', 0,0,295,208);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetY(100); 

	$pdf->SetFont( 'Arial', 'B', 60 ); 

	$pdf->Cell(0, 10, $fullName, 0, 0, 'C' );
	$pdf->SetY(160); 

	$pdf->SetFont( 'Arial', 'B', 17 ); 

	$pdf->Cell( 225, 10, $currentDate, 0, 0, 'R' ); 
	
	if($_POST['email'] != '')
	{
		$pdf->Output($path,'F');
		
		/* email certificate */
		
		$to = $_POST['email'];
		$email = new PHPMailer();
		$headers = "From:noreply@orthostudio.ca\r\n";
		$headers .= "Reply-To: noreply@orthostudio.ca\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";	
		$user_headers = "From: noreply@orthostudio.ca\r\n";
		$user_headers .= "Reply-To: noreply@orthostudio.ca\r\n";
		$user_headers .= "MIME-Version: 1.0\r\n";
		$user_headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";	

		$email->From      = 'noreply@orthostudio.ca';
		$email->FromName  = 'Children Book';
		$email->Subject   = "Well Done!";
		$email->Body      =  'Congratulation on completing all the quiz.';
		$email->IsHTML(true); 
		$email->AddAddress($to);
		$email->AddAttachment($attachments[0]);
		$email->Send();
		header("Refresh:0");
	}
	else
	{
		$pdf->Output('D','certificate.pdf');
		header("Refresh:0");
	}
    exit;
}


function as_fpdf_create_admin_page() {
?>

<?php
}